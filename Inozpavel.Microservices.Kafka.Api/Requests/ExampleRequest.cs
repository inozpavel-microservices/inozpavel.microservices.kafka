﻿using Mediator;

namespace Inozpavel.Microservices.Kafka.Api.Requests;

public class ExampleRequest : IRequest
{
}

public class ExampleRequestHandler : IRequestHandler<ExampleRequest>
{
	public ValueTask<Unit> Handle(ExampleRequest request, CancellationToken cancellationToken)
	{
		return Unit.ValueTask;
	}
}
