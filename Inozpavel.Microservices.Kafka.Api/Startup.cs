using Inozpavel.Microservices.Kafka.Api.Requests;
using Microservices.Kafka.Extensions;
using Microsoft.OpenApi.Models;
using Microservices.Kafka.BackgroundServices.Mediator.Extensions;

namespace Inozpavel.Microservices.Kafka.Api;

public class Startup
{
	private readonly IConfiguration _configuration;

	public Startup(IConfiguration configuration)
	{
		_configuration = configuration;
	}

	public void ConfigureServices(IServiceCollection services)
	{
		services.AddControllers();
		services
			.AddKafkaProducer(_configuration, "KafkaOptions")
			.AddKafkaMessageRequestConsumer<ExampleRequest>(
				_configuration, "SomeConsumer", "KafkaOptions")
			.AddKafkaSystemTextJsonSerializers();
		services.AddSwaggerGen(c =>
		{
			c.SwaggerDoc("v1", new OpenApiInfo { Title = "Inozpavel.Microservices.Kafka.Api", Version = "v1" });
		});
	}

	public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
	{
		if (env.IsDevelopment())
		{
			app.UseDeveloperExceptionPage();
			app.UseSwagger();
			app.UseSwaggerUI(c =>
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "Inozpavel.Microservices.Kafka.Api v1"));
		}

		app.UseHttpsRedirection();

		app.UseRouting();

		app.UseAuthorization();

		app.UseEndpoints(endpoints => endpoints.MapControllers());
	}
}
