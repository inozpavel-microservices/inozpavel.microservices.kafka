using Confluent.Kafka;
using Mediator;
using Microservices.Kafka.BackgroundServices.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microservices.Kafka.Options;

// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global
// ReSharper disable MemberCanBePrivate.Global

namespace Microservices.Kafka.BackgroundServices.Mediator.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddKafkaMessageRequestConsumer<TKey, TValue>(
        this IServiceCollection services,
        Action<KafkaConsumerOptions<TKey, TValue>> overrideConfigure,
        Action<KafkaConsumerOptions<TKey, TValue>> defaultConfigure) where TValue : IRequest =>
        services
            .AddKafkaMessageConsumer<MediatorKafkaMessageConsumerRequestsBackgroundService<TKey, TValue>, TKey, TValue>(
                overrideConfigure, defaultConfigure);

    public static IServiceCollection AddKafkaMessageRequestConsumer<TValue>(
        this IServiceCollection services,
        Action<KafkaConsumerOptions<Ignore, TValue>> overrideConfigure,
        Action<KafkaConsumerOptions<Ignore, TValue>> defaultConfigure) where TValue : IRequest =>
        services
            .AddKafkaMessageConsumer<MediatorKafkaMessageConsumerRequestsBackgroundService<Ignore, TValue>, Ignore, TValue>(
                overrideConfigure, defaultConfigure);

    public static IServiceCollection AddKafkaMessageRequestConsumer<TKey, TValue>(
        this IServiceCollection services,
        IConfiguration configuration,
        string overrideSectionName,
        string defaultSectionName) where TValue : IRequest =>
        services
            .AddKafkaMessageRequestConsumer<TKey, TValue>(
                configuration.GetRequiredSection(overrideSectionName).Bind,
                configuration.GetRequiredSection(defaultSectionName).Bind);

    public static IServiceCollection AddKafkaMessageRequestConsumer<TValue>(
        this IServiceCollection services,
        IConfiguration configuration,
        string overrideSectionName,
        string defaultSectionName) where TValue : IRequest =>
        services
            .AddKafkaMessageRequestConsumer<Ignore, TValue>(
                configuration.GetRequiredSection(overrideSectionName).Bind,
                configuration.GetRequiredSection(defaultSectionName).Bind);
}
