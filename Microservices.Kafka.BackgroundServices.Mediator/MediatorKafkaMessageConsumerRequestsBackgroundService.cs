﻿using Confluent.Kafka;
using Mediator;
using Microsoft.Extensions.DependencyInjection;

namespace Microservices.Kafka.BackgroundServices.Mediator;

public class MediatorKafkaMessageConsumerRequestsBackgroundService<TKey, TValue> :
    KafkaSingleMessageConsumerBackgroundService<TKey, TValue>
    where TValue : IRequest
{
    private readonly IServiceProvider _serviceProvider;

    public MediatorKafkaMessageConsumerRequestsBackgroundService(IServiceProvider serviceProvider)
        : base(serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    protected override async Task Handle(ConsumeResult<TKey, TValue> message, CancellationToken cancellationToken)
    {
        var request = message.Message.Value;

        if (request == null)
        {
            return;
        }

        using var scope = _serviceProvider.CreateScope();
        var mediator = scope.ServiceProvider.GetRequiredService<IMediator>();

        await mediator.Send(request, cancellationToken);
    }
}
