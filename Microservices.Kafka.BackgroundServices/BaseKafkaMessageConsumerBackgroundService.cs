﻿using System.Threading.Channels;
using Confluent.Kafka;
using Microservices.Kafka.Abstractions;
using Microservices.Kafka.Options;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Microservices.Kafka.BackgroundServices;

public abstract class BaseKafkaMessageConsumerBackgroundService<TKey, TValue> : BackgroundService
{
    protected readonly ILogger<BaseKafkaMessageConsumerBackgroundService<TKey, TValue>> Logger;
    private readonly IDeserializer<TKey> _keyDeserializer;
    private readonly IDeserializer<TValue> _valueDeserializer;
    protected readonly KafkaConsumerOptions<TKey, TValue> ConsumerOptions;
    private Channel<ConsumeResult<TKey, TValue>> _processingChannel;
    private Channel<ConsumeResult<TKey, TValue>> _committingChannel;
    private CancellationTokenSource _consumerTokenSource;
    private Task _consumingTask;
    private Task _committingTask;
    private Task _processingTask;
    private readonly IKafkaProducer<byte[], byte[]> _retryProducer;
    private IConsumer<TKey, TValue> _consumer;

    public override async Task StopAsync(CancellationToken cancellationToken)
    {
        _consumerTokenSource.Cancel();

        await base.StopAsync(cancellationToken);
        
        CommitAllStoredOffsets();
        
        _consumer.Close();
    }

    protected BaseKafkaMessageConsumerBackgroundService(IServiceProvider serviceProvider)
    {
        Logger = serviceProvider
            .GetRequiredService<ILogger<BaseKafkaMessageConsumerBackgroundService<TKey, TValue>>>();
        _keyDeserializer = serviceProvider.GetRequiredService<IDeserializer<TKey>>();
        _valueDeserializer = serviceProvider.GetRequiredService<IDeserializer<TValue>>();
        ConsumerOptions = serviceProvider.GetRequiredService<IOptions<KafkaConsumerOptions<TKey, TValue>>>().Value;
        _retryProducer = serviceProvider.GetRequiredService<IKafkaProducer<byte[], byte[]>>();
    }

    protected string LogPrefix { get; } = $"KafkaConsumer<{typeof(TKey).Name}, {typeof(TValue).Name}> ";

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _consumerTokenSource = new CancellationTokenSource();
        _processingChannel =
            Channel.CreateBounded<ConsumeResult<TKey, TValue>>(ConsumerOptions.ProcessingChannelCapacity);
        _committingChannel = Channel.CreateUnbounded<ConsumeResult<TKey, TValue>>();

        ConsumerOptions.EnableAutoCommit = true;
        ConsumerOptions.EnableAutoOffsetStore = false;

        _consumer = new ConsumerBuilder<TKey, TValue>(ConsumerOptions)
            .SetKeyDeserializer(_keyDeserializer)
            .SetValueDeserializer(_valueDeserializer)
            .SetPartitionsAssignedHandler((_, list) =>
                Logger.LogInformation("{LogPrefix} Assigned: {@Details}", LogPrefix, list))
            .SetPartitionsRevokedHandler((_, list) =>
                Logger.LogInformation("{LogPrefix} Revoked: {@Details}", LogPrefix, list))
            .SetErrorHandler((_, error) =>
                Logger.LogError("{LogPrefix} Kafka error: {@Error}", LogPrefix, error))
            .SetLogHandler((_, message) => Logger.Log(
                (LogLevel)message.LevelAs(LogLevelType.MicrosoftExtensionsLogging),
                "{LogPrefix} Kafka message: {@message}", LogPrefix, message.Message))
            .Build();

        _consumingTask = Task.Run(
            () => FillProcessingChannel(_consumer, _processingChannel.Writer, stoppingToken),
            stoppingToken);
        _committingTask = Task.Run(
            () => EmptyCommittingChannel(_consumer, _committingChannel.Reader, stoppingToken),
            stoppingToken);
        _processingTask = Task.Run(
            () => ProcessMessages(_processingChannel.Reader, _committingChannel.Writer, stoppingToken),
            stoppingToken);

        return Task.WhenAll(_consumingTask, _committingTask, _processingTask);
    }

    protected abstract Task ProcessMessages(
        ChannelReader<ConsumeResult<TKey, TValue>> processingChannel,
        ChannelWriter<ConsumeResult<TKey, TValue>> committingChannel,
        CancellationToken cancellationToken);

    private async Task FillProcessingChannel(
        IConsumer<TKey, TValue> consumer,
        ChannelWriter<ConsumeResult<TKey, TValue>> processingChannel,
        CancellationToken cancellationToken)
    {
        consumer.Subscribe(ConsumerOptions.Topic);

        while (!cancellationToken.IsCancellationRequested)
        {
            try
            {
                var message = consumer.Consume(cancellationToken);

                if (message == null)
                {
                    continue;
                }

                await processingChannel.WriteAsync(message, cancellationToken);
            }
            catch (ConsumeException e)
            {
                Logger.LogError(e, "{LogPrefix} Consume exception", LogPrefix);

                if (string.IsNullOrWhiteSpace(ConsumerOptions.RetryTopic))
                {
                    continue;
                }

                await _retryProducer.Produce(
                    ConsumerOptions.RetryTopic,
                    e.ConsumerRecord.Message.Key,
                    e.ConsumerRecord.Message.Value);
            }
            catch (OperationCanceledException)
            {
                // ignore
            }
            catch (Exception e)
            {
                Logger.LogError(e, "{LogPrefix} Consume exception", LogPrefix);
            }
        }

        processingChannel.Complete();
    }

    private async Task EmptyCommittingChannel(
        IConsumer<TKey, TValue> consumer,
        ChannelReader<ConsumeResult<TKey, TValue>> committingChannel,
        CancellationToken cancellationToken)
    {
        while (!committingChannel.Completion.IsCompleted)
        {
            try
            {
                var message = await committingChannel.ReadAsync(cancellationToken);
                consumer.StoreOffset(message);
            }
            catch (OperationCanceledException)
            {
                // ignore
            }
            catch (ChannelClosedException)
            {
                // ignore
            }
            catch (Exception e)
            {
                Logger.LogError(e, "{LogPrefix} Store offset error", LogPrefix);
            }
        }
    }

    private void CommitAllStoredOffsets()
    {
        try
        {
            if (_consumer.Assignment
                .Select(_consumer.Position)
                .Any(offset => offset == Offset.Stored))
            {
                _consumer.Commit();
            }
        }
        catch (Exception e)
        {
            Logger.LogError(e, "{LogPrefix} Kafka commit error", LogPrefix);
        }
    }
}
