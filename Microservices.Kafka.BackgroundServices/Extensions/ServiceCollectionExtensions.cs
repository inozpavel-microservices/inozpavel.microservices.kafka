﻿using Microservices.Kafka.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global
// ReSharper disable MemberCanBePrivate.Global

namespace Microservices.Kafka.BackgroundServices.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddKafkaMessageConsumer<TService, TKey, TValue>(
        this IServiceCollection services,
        Action<KafkaConsumerOptions<TKey, TValue>> overrideConfigure,
        Action<KafkaConsumerOptions<TKey, TValue>> defaultConfigure)
        where TService : KafkaSingleMessageConsumerBackgroundService<TKey, TValue>
    {
        services
            .Configure(MakeInheritance(overrideConfigure, defaultConfigure))
            .AddHostedService<TService>();

        return services;
    }

    public static IServiceCollection AddKafkaMessageConsumer<TService, TKey, TValue>(
        this IServiceCollection services,
        IConfiguration configuration,
        string overrideConfigureSectionName,
        string defaultConfigureSectionName)
        where TService : KafkaSingleMessageConsumerBackgroundService<TKey, TValue> =>
        services
            .AddKafkaMessageConsumer<TService, TKey, TValue>(
                configuration.GetRequiredSection(overrideConfigureSectionName).Bind,
                configuration.GetRequiredSection(defaultConfigureSectionName).Bind);

    private static Action<T> MakeInheritance<T>(Action<T> overrideConfigure, Action<T> defaultConfigure) =>
        defaultConfigure + overrideConfigure;
}
