using System.Threading.Channels;
using Confluent.Kafka;
using Microservices.Kafka.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Microservices.Kafka.BackgroundServices;

public abstract class KafkaSingleMessageConsumerBackgroundService<TKey, TValue> :
    BaseKafkaMessageConsumerBackgroundService<TKey, TValue>
{
    private readonly IKafkaProducer<TKey, TValue> _handleRetryProducer;

    protected KafkaSingleMessageConsumerBackgroundService(IServiceProvider serviceProvider) : base(serviceProvider)
    {
        _handleRetryProducer = serviceProvider.GetRequiredService<IKafkaProducer<TKey, TValue>>();
    }

    protected override async Task ProcessMessages(
        ChannelReader<ConsumeResult<TKey, TValue>> processingChannel,
        ChannelWriter<ConsumeResult<TKey, TValue>> committingChannel,
        CancellationToken cancellationToken)
    {
        while (!processingChannel.Completion.IsCompleted && !cancellationToken.IsCancellationRequested)
        {
            try
            {
                var message = await processingChannel.ReadAsync(cancellationToken);

                try
                {
                    await Handle(message, cancellationToken);
                }
                catch (Exception) when (cancellationToken.IsCancellationRequested)
                {
                    break;
                }
                catch (Exception e)
                {
                    Logger.LogError(e, "{LogPrefix} Handle error", LogPrefix);

                    await committingChannel.WriteAsync(message, cancellationToken);

                    if (string.IsNullOrWhiteSpace(ConsumerOptions.RetryTopic) || message == null)
                    {
                        continue;
                    }

                    await _handleRetryProducer.Produce(
                        ConsumerOptions.RetryTopic,
                        message.Message.Key,
                        message.Message.Value);
                }

                await committingChannel.WriteAsync(message, cancellationToken);
            }
            catch (OperationCanceledException)
            {
                // ignore
            }
            catch (ChannelClosedException)
            {
                // ignore
            }
            catch (Exception e)
            {
                Logger.LogError(e, "{LogPrefix} Internal kafka error", LogPrefix);
            }
        }

        committingChannel.Complete();
    }

    protected abstract Task Handle(ConsumeResult<TKey, TValue> message, CancellationToken cancellationToken);
}
