// ReSharper disable TypeParameterCanBeVariant

namespace Microservices.Kafka.Abstractions;

public interface IKafkaProducer<TKey, TValue>
{
    Task Produce(string topic, TKey key, TValue value);

    Task Produce(string topic, TValue value);
}
