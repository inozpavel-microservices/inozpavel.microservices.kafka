using Confluent.Kafka;

namespace Microservices.Kafka.CustomSerializers;

public class IgnoreBytesSerializer<T> :
    ISerializer<T>,
    IDeserializer<T>
{
    public byte[] Serialize(T data, SerializationContext context) => default;

    public T Deserialize(ReadOnlySpan<byte> data, bool isNull, SerializationContext context) => default;
}
