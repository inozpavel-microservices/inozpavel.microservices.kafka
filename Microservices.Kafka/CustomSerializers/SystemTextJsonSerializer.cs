using System.Text.Json;
using System.Text.Json.Serialization;
using Confluent.Kafka;

namespace Microservices.Kafka.CustomSerializers;

internal class SystemTextJsonSerializer<T> :
    ISerializer<T>,
    IDeserializer<T>
{
    private readonly JsonSerializerOptions _serializerOptions = new ()
    {
        NumberHandling = JsonNumberHandling.AllowReadingFromString,
        PropertyNameCaseInsensitive = true,
        DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
    };

    public byte[] Serialize(T data, SerializationContext context) =>
        data is null
            ? null
            : JsonSerializer.SerializeToUtf8Bytes(data, _serializerOptions);

    public T Deserialize(ReadOnlySpan<byte> data, bool isNull, SerializationContext context) =>
        isNull
            ? default
            : JsonSerializer.Deserialize<T>(data, _serializerOptions);
}
