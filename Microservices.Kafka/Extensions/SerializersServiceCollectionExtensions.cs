using Confluent.Kafka;
using Microservices.Kafka.CustomSerializers;
using Microsoft.Extensions.DependencyInjection;

// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedType.Global
// ReSharper disable MemberCanBePrivate.Global

namespace Microservices.Kafka.Extensions;

public static class SerializersServiceCollectionExtensions
{
    public static IServiceCollection AddKafkaSystemTextJsonSerializers(this IServiceCollection services) =>
        services
            .AddSingleton(typeof(ISerializer<>), typeof(SystemTextJsonSerializer<>))
            .AddSingleton(typeof(IDeserializer<>), typeof(SystemTextJsonSerializer<>))
            .AddKafkaEmptyTypesSerializers()
            .AddKafkaDefaultSerializers();

    public static IServiceCollection AddKafkaDefaultSerializers(this IServiceCollection services) =>
        services
            .AddSingleton(Serializers.ByteArray)
            .AddSingleton(Deserializers.ByteArray)
            .AddSingleton(Serializers.Int32)
            .AddSingleton(Deserializers.Int32)
            .AddSingleton(Serializers.Int64)
            .AddSingleton(Deserializers.Int64)
            .AddSingleton(Serializers.Double)
            .AddSingleton(Deserializers.Double)
            .AddSingleton(Serializers.Single)
            .AddSingleton(Deserializers.Single);

    public static IServiceCollection AddKafkaEmptyTypesSerializers(this IServiceCollection services) =>
        services
            .AddSingleton(typeof(ISerializer<Ignore>), typeof(IgnoreBytesSerializer<Ignore>))
            .AddSingleton(typeof(IDeserializer<Ignore>), typeof(IgnoreBytesSerializer<Ignore>))
            .AddSingleton(typeof(ISerializer<Null>), typeof(SystemTextJsonSerializer<Null>))
            .AddSingleton(typeof(IDeserializer<Null>), typeof(SystemTextJsonSerializer<Null>));
}
