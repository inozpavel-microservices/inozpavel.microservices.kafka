using Microservices.Kafka.Abstractions;
using Microservices.Kafka.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
// ReSharper disable UnusedType.Global

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global

namespace Microservices.Kafka.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddKafkaProducer(
        this IServiceCollection services,
        Action<KafkaProducerOptions> configure) =>
        services
            .AddSingleton(typeof(IKafkaProducer<,>), typeof(KafkaProducer<,>))
            .Configure(configure);

    public static IServiceCollection AddKafkaProducer(
        this IServiceCollection services,
        IConfiguration configuration,
        string producerSectionName) =>
        services.AddKafkaProducer(configuration.GetRequiredSection(producerSectionName).Bind);
}
