using Confluent.Kafka;
using Microservices.Kafka.Abstractions;
using Microservices.Kafka.Options;
using Microsoft.Extensions.Options;

namespace Microservices.Kafka;

internal class KafkaProducer<TKey, TValue> : IDisposable, IKafkaProducer<TKey, TValue>
{
    private readonly IProducer<TKey, TValue> _producer;

    public KafkaProducer(
        IOptions<KafkaProducerOptions> producerOptions,
        ISerializer<TKey> keySerializer,
        ISerializer<TValue> valueSerializer)
    {
        _producer = new ProducerBuilder<TKey, TValue>(producerOptions.Value)
            .SetKeySerializer(keySerializer)
            .SetValueSerializer(valueSerializer)
            .Build();
    }

    public Task Produce(string topic, TKey key, TValue value) => _producer.ProduceAsync(topic, new Message<TKey, TValue>
    {
        Key = key,
        Value = value,
        Timestamp = new Timestamp(DateTimeOffset.UtcNow)
    });

    public Task Produce(string topic, TValue value) => _producer.ProduceAsync(topic, new Message<TKey, TValue>
    {
        Value = value,
        Timestamp = new Timestamp(DateTimeOffset.UtcNow)
    });

    public void Dispose()
    {
        _producer.Flush();
        _producer?.Dispose();
    }
}
