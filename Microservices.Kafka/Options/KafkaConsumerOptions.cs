using Confluent.Kafka;

// ReSharper disable UnusedTypeParameter

namespace Microservices.Kafka.Options;

public class KafkaConsumerOptions<TKey, TValue> : ConsumerConfig
{
	public KafkaConsumerOptions()
	{
#if DEBUG
		SessionTimeoutMs = 7_000;
#else
		SessionTimeoutMs = 15_000;
#endif
	}

	public string Topic { get; set; }

	public string RetryTopic { get; set; }

	public int ProcessingChannelCapacity { get; set; } = 100;
}
