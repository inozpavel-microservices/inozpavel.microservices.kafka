﻿// ReSharper disable UnusedType.Global
// ReSharper disable UnusedMember.Global

namespace Microservices.Kafka.Transfers;

public class EventKey
{
	public Guid Id { get; set; } = new ();
}
